import { Component, OnInit, ViewChild } from '@angular/core';
import {FLIGHTS} from './fligt-mockup';
import { Flight } from '../Flight';
import {MatSort, MatTableDataSource } from '@angular/material';
import {MatPaginator} from '@angular/material/paginator';
import { Tourist } from '../Tourist';
import { TouristsComponent } from '../tourists/tourists.component';
import { TOURISTS } from '../tourists/tourist-mockup';

// let FLIGHTS1: Flight[] = [
//   {id: 1, departureTime: 1589970000,arrivalTime: 1591024515, title: 'Mercury', ticketPrice: 20000, numberOfSeats: 100},
//   {id: 2, departureTime: 1589970000,arrivalTime: 1591024515, title: 'Mercury', ticketPrice: 20000, numberOfSeats: 100},
//   {id: 3, departureTime: 1589970000,arrivalTime: 1591024515, title: 'Mercury', ticketPrice: 20000, numberOfSeats: 100},
//   {id: 4, departureTime: 1589970000,arrivalTime: 1591024515, title: 'Mercury', ticketPrice: 20000, numberOfSeats: 100, tourist: 12},
//   {id: 5, departureTime: 1589970000,arrivalTime: 1591024515, title: 'Mercury', ticketPrice: 20000, numberOfSeats: 100, tourist: 12},
//   {id: 6, departureTime: 1589970000,arrivalTime: 1591024515, title: 'Mercury', ticketPrice: 20000, numberOfSeats: 100, tourist: 12}

// ];


@Component({
  selector: 'app-flights',
  templateUrl: './flights.component.html',
  styleUrls: ['./flights.component.css']
})




export class FlightsComponent implements OnInit {
  flights1 = FLIGHTS;
  dataSource;
  displayedColumns = [];
  @ViewChild(MatSort, {static: true }) sort: MatSort;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  columnNames = [
  {
    id: "title",
    value: "Title"
  }, 
  {
    id: "ticketPrice",
    value: "Price"
  },
  {
    id: "numberOfSeats",
    value: "Availability"
  },
  {
    id: "departureTime",
    value: "Departure"
  },
  {
    id: "arrivalTime",
    value: "Arrival"
  },
  {
    id: "tourist",
    value: "Tourist"
  },
  {
    id: "action",
    value: "Action"
  }
  ];
  ngOnInit() {
    this.displayedColumns = this.columnNames.map(x => x.id);
    this.dataSource = new MatTableDataSource(this.flights1);
    // let datSour = JSON.parse(this.dataSource);
    // datSour.forEach(function(e){
    //   if (typeof e === "object" ){
    //     e["action"] = "<button>Add</button><button>Edit</button>";
    //   }
    // });
    // console.log(this.dataSource);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  
  // columnNames = ["Title", "Price", "Available seats", "Departure Time", "Arrival Time", "Tourist"];
  constructor() { }
 

}
