import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable }     from 'rxjs';
import { switchMap, min }      from 'rxjs/operators';
import { FLIGHTS } from '../fligt-mockup';
import { Flight } from 'src/app/Flight';
import { TOURISTS } from 'src/app/tourists/tourist-mockup';
import { fromUnixTimeStampToDate, fromDateToUnixTimeStamp } from 'src/app/UNIXtimeStampConverter';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-edit-flight',
  templateUrl: './edit-flight.component.html',
  styleUrls: ['./edit-flight.component.css']
})
export class EditFlightComponent implements OnInit {
  selectedId:number;
  flights = FLIGHTS;
  tourists = [];  
  private t = TOURISTS.forEach(element => { this.tourists.push(element.toString());  });
  model:Flight;
  newDepartureTime:Date;
  newArrivalTime:Date;
  constructor(private route: ActivatedRoute) { 
      this.newDepartureTime = null;
      this.newArrivalTime = null;
  }
  
  ngOnInit() {
    this.route.params.subscribe(params => {
      this.selectedId = params['id'];
      this.model = this.flights[this.selectedId-1];
      // for (const key in this.flights) {
      //   if (this.flights.hasOwnProperty(key)) {
      //      this.model = this.flights[key];
      //   }
      // }
    });
  } 
  submitted = false;

  updateDate(date:string, varToSign:string){
    let d = new Date();
    console.log("date: " + date);
    let year = Number(date.substring(0,4));
    let month = Number(date.substring(5,7));
    let day = Number(date.substring(8,10));
    let hour = Number(date.substring(11,13));
    let minute = Number(date.substring(14,16));
    
    d.setFullYear(year, month-1, day);
    d.setHours(hour, minute);
    //console.log("dateInMilisec: " + d.toString());
    if(varToSign =='departureTime')
      this.newDepartureTime = d;
    if(varToSign == 'arrivalTime')
      this.newArrivalTime = d;
  }
  
  onSubmit() { this.submitted = true;

    console.log(this.model);
    console.log("DATE: " + fromUnixTimeStampToDate(this.model.departureTime*1000));
    if(this.newDepartureTime){this.model.departureTime = this.newDepartureTime.getTime()/1000;this.newDepartureTime=null;}
    if(this.newArrivalTime){this.model.arrivalTime = this.newArrivalTime.getTime()/1000;this.newArrivalTime=null;}
  }


}


