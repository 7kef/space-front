import {Flight} from '../Flight';

let f1 = new Flight(1,"Mercury", 1589970000, 1591024515, 100, 500000);
let f2 = new Flight(2, "Moon", 1589970000, 1591024515, 50, 150000);
let f3 = new Flight(3, "Jupiter", 1589970000, 1591024515, 20, 1750000);
let f4 = new Flight(4, "Mars", 1589970000, 1591024515, 500, 220000);
let f5 = new Flight(5, "Saturn", 1589970000, 1591024515, 300, 82000000);
let f6 = new Flight(6, "Neptune", 1589970000, 1591024515, 150, 62000000);
let f7 = new Flight(7, "Uranus", 1589970000, 1591024515, 10, 50000000);

export const FLIGHTS:Flight[] = [f1,f2,f3,f4,f5,f6,f7];