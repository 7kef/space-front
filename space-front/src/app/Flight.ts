import { Tourist } from './Tourist';

export class Flight{
    id : number;
    title : string;
    departureTime : number;
    arrivalTime : number;
    numberOfSeats : number;
    ticketPrice : number;
    tourist : Tourist[];

    constructor(id:number, title: string, departureTime : number, arrivalTime: number, numberOfSeats: number, ticketPrice: number){
        this.id = id;
        this.title = title;
        this.departureTime = departureTime;
        this.arrivalTime = arrivalTime;
        this.numberOfSeats = numberOfSeats;
        this.ticketPrice = ticketPrice;
        this.tourist = [];
    }
    addTourist(newTourist: Tourist){
        if(this.tourist.length < this.numberOfSeats){
            this.tourist.push(newTourist);
        }
    }


}