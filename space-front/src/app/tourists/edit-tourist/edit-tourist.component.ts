import { Component, OnInit } from '@angular/core';
import { TOURISTS } from '../tourist-mockup';
import { Tourist } from 'src/app/Tourist';
import { ActivatedRoute } from '@angular/router';
import { Gender } from 'src/app/Gender';
import sampleData from '../../../assets/countries.json';

@Component({
  selector: 'app-edit-tourist',
  templateUrl: './edit-tourist.component.html',
  styleUrls: ['./edit-tourist.component.css']
})
export class EditTouristComponent implements OnInit {
  selectedId:number;
  tourists = TOURISTS; 
  gender = [Gender.MALE, Gender.FEMALE, Gender.OTHER];
  model:Tourist;
  countries = [];
  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.selectedId = params['id'];
      this.model = this.tourists[this.selectedId-1];  
      // for (const key in this.tourists) {
      //   if (this.tourists.hasOwnProperty(key)) {
      //      this.model = this.tourists[key];
      //   }
      // }
    });

    sampleData.forEach(element => {
      this.countries.push(element.name);
    });
  }

  submitted = false;

}
