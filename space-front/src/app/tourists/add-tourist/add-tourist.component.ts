import { Component, OnInit, Injectable } from '@angular/core';
import { Tourist } from 'src/app/Tourist';
import { Gender } from 'src/app/Gender';
import sampleData from '../../../assets/countries.json';


@Component({
  selector: 'app-add-tourist',
  templateUrl: './add-tourist.component.html',
  styleUrls: ['./add-tourist.component.css']
})

export class AddTouristComponent implements OnInit {

  countries = [];
  gender = [Gender.MALE, Gender.FEMALE, Gender.OTHER];
  model = new Tourist(1, '','',Gender.MALE, '','',0);

  submitted = false;
  
  constructor(){

  }

  ngOnInit() {
    sampleData.forEach(element => {
      this.countries.push(element.name);
    });
   // console.log(this.countries);
  }
  


  onSubmit() { this.submitted = true; }

  // TODO: Remove this when we're done
  get diagnostic() { return JSON.stringify(this.model); }

  newTourist() {
    this.model = new Tourist(1, '','',Gender.MALE, '','',0);
  }


  

}
